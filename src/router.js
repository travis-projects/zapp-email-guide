import Vue from 'vue'
import Router from 'vue-router'

// * Import Views
import Start from './views/Start'
import Setup from './views/Setup'
import Finish from './views/Finish'
import Signature from './views/Signature'

// * Import components
import SetupIndex from './components/Setup/index'
import SetupDesktop from './components/Setup/Desktop/Desktop'
import SetupMobile from './components/Setup/Mobile/Mobile'
import DesktopSetupWizard from './components/Setup/Desktop/SetupWizard'
import MobileSetupWizard from './components/Setup/Mobile/SetupWizard'
// Webmail
import SetupWebmail from './components/Setup/Webmail/Webmail'
import WebmailGuide from './components/Setup/Webmail/Guide'

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: 'start',
    component: Start
  },
  {
    path: '/setup',
    component: Setup,
    children: [
      {
        path: '/',
        name: 'SetupIndex',
        component: SetupIndex
      },
      {
        path: 'desktop',
        name: 'SetupDesktop',
        component: SetupDesktop
      },
      {
        path: 'desktop/:client',
        name: 'DesktopSetupWizard',
        component: DesktopSetupWizard
      },
      {
        path: 'mobile',
        name: 'SetupMobile',
        component: SetupMobile
      },
      {
        path: 'mobile/:client',
        name: 'MobilesetupWizard',
        component: MobileSetupWizard
      },
      {
        path: 'webmail',
        name: 'SetupWebmail',
        component: SetupWebmail,
        children: [
          {
            path: 'guide',
            name: 'WebmailGuide',
            component: WebmailGuide
          }
        ]
      }
    ]
  },
  {
    path: '/finish',
    name: 'finish',
    component: Finish
  },
  {
    path: '/signature',
    name: 'signature',
    component: Signature
  }
]

export default new Router({
  routes: routes
})
