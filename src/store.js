import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {},
    guides: {}
  },
  mutations: {
    setGuides (state, payload) {
      state.guides.allDesktopEmailGuides = payload.allDesktopEmailGuides
      state.guides.allMobileEmailGuides = payload.allMobileEmailGuides
    },
    setUser (state, payload) {
      state.user = payload
    }
  },
  actions: {
    userData (context, payload) {
      payload.email = `${payload.email}@${payload.company.domain}`
      context.commit('setUser', payload)
    }
  }
})
