import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import Buefy from 'buefy'
import './assets/scss/app.scss'
import { createProvider } from './vue-apollo'
import VeeValidate, { Validator } from 'vee-validate'

const dictionary = {
  custom: {
    email: {
      regex: 'Invalid email. Please enter your username without the @zapp-uk.com domain.'
    }
  }
}
const veeValidateConfig = {
  events: 'input|blur'
}

Validator.localize('en', dictionary)
Vue.use(VeeValidate, veeValidateConfig)
Vue.use(Buefy)

Vue.filter('replaceDomain', (markdown, domain) => {
  return markdown.replace('zapp-uk.com', domain)
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  provide: createProvider().provide(),
  render: h => h(App)
}).$mount('#app')
