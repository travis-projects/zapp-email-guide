module.exports = {
  lintOnSave: true,

  pwa: {
    name: 'Zapp Email Setup Guide',
    themeColor: '#ea5b0c',
    msTileColor: '#3c3c3c'
  }
}
